#![no_std]
#![no_main]

use panic_halt as _;
use ufmt;
//use energy_data_types::base_types::DataType;

static SAMPLE_INTERVAL: u16 = 1; //1ms for 20 amples per cycle = 50 Hz


//static FIXED_SAMPLE: u16 = 512;

#[arduino_hal::entry]
fn main() -> ! {


    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);

    //flash this to show signs of life!!
    let mut led = pins.d13.into_output();

    let mut adc = arduino_hal::Adc::new(dp.ADC, Default::default());

    let _a0 = pins.a0.into_analog_input(&mut adc);

    //Initial smaple
    //let mut a_0;

    //20 samples
    let mut test_sin_sample1: [i16; 20] = [0; 20];

    test_sin_sample1[0] = 512;
    test_sin_sample1[1] = 670;
    test_sin_sample1[2] = 812;
    test_sin_sample1[3] = 926;
    test_sin_sample1[4] = 998;
    test_sin_sample1[5] = 1024;
    test_sin_sample1[6] = 998;
    test_sin_sample1[7] = 926;
    test_sin_sample1[8] = 812;
    test_sin_sample1[9] = 670;
    test_sin_sample1[10] = 512;
    test_sin_sample1[11] = 354;
    test_sin_sample1[12] = 212;
    test_sin_sample1[13] = 98;
    test_sin_sample1[14] = 26;
    test_sin_sample1[15] = 0;
    test_sin_sample1[16] = 26;
    test_sin_sample1[17] = 98;
    test_sin_sample1[18] = 212;
    test_sin_sample1[19] = 354;

    let mut samp_index: usize = 0;
    

    //Sample loop
    loop {
         


        //Indicate the loop is running
        led.toggle();

        //Sample the sensor
        //a_0 = a0.analog_read(&mut adc);

        let _dummy_sample: u16 =  512 as u16;

        //Report the sensor data
        let _start = ufmt::uwrite!(&mut serial, "S");

        //let _type = ufmt::uwrite!(&mut serial, "{:#?}", DataType::CurrentAC as isize);

        //Actual sample
        //let _message = ufmt::uwrite!(&mut serial, "{:#?} ", a_0);

        // Sin wave sample 0 .. 1024
        let _message = ufmt::uwrite!(&mut serial, "{:#?} ", test_sin_sample1[samp_index]);

        //Fixed sample
        //let _message = ufmt::uwrite!(&mut serial, "{:#?} ", FIXED_SAMPLE);

        let _end = ufmt::uwrite!(&mut serial, "\n");

        if samp_index >= 19 {
            samp_index = 0;
        } else {
            samp_index += 1;
        }

        arduino_hal::delay_ms(SAMPLE_INTERVAL);
    }
}
