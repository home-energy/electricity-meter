#! /bin/bash

#
# Installer script for embedded rust dev
#
#

#Set up user and groups first


#Locations
TOOLS=tools
PROJECTS=projects
PROJECT=electricity-meter

echo 'export TOOLS' | tee -a ~/.bashrc
echo 'export PROJECTS' | tee -a ~/.bashrc
echo 'export PROJECT=electricity-meter' | tee -a ~/.bashrc
echo 'export TOOLS=$HOME/tools' | tee -a ~/.bashrc
echo 'export PATH=$PATH:$TOOLS' | tee -a ~/.bashrc

source ~/.bashrc

cd $HOME
mkdir $TOOLS
mkdir $PROJECTS


sudo apt update

# basic tools for other installs
sudo apt --assume-yes install software-properties-common apt-transport-https wget

# vscode 
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
# sudo add-apt-repository  "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" -y 
sudo add-apt-repository -y "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt --assume-yes install code

# Install rustup
curl https://sh.rustup.rs -sSf > install-rust.sh
chmod u+x install-rust.sh
./install-rust.sh -y
source $HOME/.cargo/env

# Add .cargo to the path
echo 'export PATH=$PATH:$HOME/.cargo' | tee -a ~/.bashrc
source ~/.bashrc

# Bin Utils
cargo install cargo-binutils

rustup component add llvm-tools-preview

rustup target add  wasm32-unknown-unknown

# We need a nightly for embedded
rustup default nightly


# Rust
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
 
# Firefox
sudo apt --assume-yes install firefox

# GCC
apt --assume-yes install gcc
apt --assume-yes install gcc-avr
apt --assume-yes install avr-libc


# The Emulator
apt --assume-yes install qemu-system-misc

# The AVR Debugger
apt --assume-yes install gdb-avr

# avrdude allows the device to be flashed
apt --assume-yes install avrdude

# cu for watching the output of the arduino on the usb of the host
apt --assume-yes install cu

# For some reason, the var PROJECTS can't be used to make a directory!!!
# mkdir projects
cd $PROJECTS
git clone https://sbnlocean@bitbucket.org/softwarebynumbers/electricity-meter.git
cd $PROJECT
cargo build
