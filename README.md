# Electricity Meter

A mains current data acquisition unit based on an Arduino and a current clamp.

Acquires samples from the 10 bit ADC on the Arduino board.  An offset of 0.5 V is applied allowing for the negative and positive swings of the current reading. 

Project has the following objectives:

* Capture current values
* Capture current sense (export/import/balance)
* Present the sampled data over a tty port on usb.

This project is a single part of a five project effort that includes:

* Electricity Meter - this project
* Energy Web - WASM web application to display, summarise and control collected energy data
* Energy Server - to acquire energy data from sources, record it and make it available on a web service
* Energy Proxy - to provide consistent naming and a single port to the web services
* Energy Types - A common type system representing data types for serialisation etc.

In this context, the objective is to make information about the whole energy performance of a house as visible as possible using simple measurements, low cost micro controllers and cheap displays, written entirely in Rust.

__Project Locations__

Projects are hosted on bit bucket at the following urls:

* electricity-meter https://bitbucket.org/home-energy/electricity-meter 
* energy-web        https://bitbucket.org/home-energy/energy-web
* energy-server     https://bitbucket.org/home-energy/energy-server
* energy-proxy      private
* energy-types      https://bitbucket.org/home-energy/energy-types


Code is built from a template from here https://github.com/Rahix/avr-hal-template.git as noted below.

A 30A SCT0103 current clamp sensor is used to feed an analoge of the grid draw current into A0. This sensor is oversized for household use, a 10 Amp sensor would have been better, or perhaps both.

Note:
* When we are generating (ie PV system is exporting) the data is zero.  Maybe a pve bias on the 
  input or the reference voltage would fix this.

## Build And Run

The development environment for this project (and other associated projects) is run inside an lxc container.  Because the system comprises of embedded, server and wasm targets, it is easier to keep the dev environments isolated.  This also means that the environments can be built completely from scripts that are version controlled.  A downside is that the environments are linux specific.  The projects can be treated like any other rust dev projects, take care of the state of tooling and VS Code plugins.

In this project and associated projects I use Terraform and a build script to create and destroy the development and runtime environments.

### Create The Container

This will create a new container, install tools, checkout and build the project.

```bash
terraform init
terraform apply
```

### To Shell Into An LXC Container

```bash
lxc exec electricity-meter bash 

```

### Run VS Code From Within The Container

```bash
code  --no-sandbox --user-data-dir /tmp
```

### To build inside the container

The dev version with all symbols:
```
  cargo build //Might run out of register space
  cargo build --release //if the above fails.
```

__NOTE__  Remember that the target is set in the .cargo/config.toml file, not the Cargo.toml

## Debug On qemu-system-avr

We can build and debug (without inputs) the system on a simulator.  Note we use an Uno here, its equivalent to the Duemilanove and easier to type!!
```
qemu-system-avr -serial tcp::5678,server=on,wait=off -machine uno -cpu avr5-avr-cpu -bios target/avr-atmega328p/debug/ard2.elf -s -S
```

The data output can be captured from the USB output using cu

```
cu -l /dev/ttyUSB0 -s 57600
```

The debugger is run as 

```
avr-gdb target/avr-atmega328p/debug/ard2.elf
```
```
target remote :1234
b main
layout src
   or
layout split to see the assembler
c continue
n next
quit when done
```

## To Build And Flash

This builds and then flashes the elf to the board
```
./flash.sh target/avr-atmega328p/debug/electricity-meter.elf
```

To find the USB port (on linux)

```
dmesg | grep -i USB
```


Make sure you have a /dev/USB0 or /dev/ttyUSBx that matches the host /dev/ttyUSB0 (or x).
If the container does not have say /dev/ttyUSB0 the create it using the following __FROM THE HOST!!__:

```
lsusb
```