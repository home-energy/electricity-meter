terraform {
  required_providers {
    lxd = {
      source = "terraform-lxd/lxd"
      version = "1.7.1"
    }
  }
}

# provider to connect to infrastructure
provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}


variable "name" {
  description = "The name of instance"
  default = "electricity-meter"
}

variable "storage-name" {
  description = "Name of the storage system"
  default = "embed-rust-pool"
}

variable "volume-name" {
  description = "Name of the storage system"
  default = "embed-rust-volume"
}

variable "network-name" {
  description = "Name of the network system"
  default = "embed-rust-nt"
}

variable "profile-name" {
  description = "Name of the profile"
  default = "embed-rust-profile"
}

#
# This is a problem, we have to recreate the gui profile with a 
# new name because terraform thinks it already exists!!
#
variable "gui-profile-name" {
  description = "Name of the profile"
  default = "gui_profile1"
}
variable "installer" {
  description = "An initialisation script for the instance"
  default = "install.sh"
}


variable "ip" {
  description = "An alternate ip address for the instance"  
  default = "10.150.19.1/24"
}

# This module gives us x windows within our dev container 
# Notem url must be non-prefixed, ie dont include the username
module "gui" {
#  source = "git::https://bitbucket.org/softwarebynumbers/terraform-lxc-gui.git"
   source = "./modules/common-profiles"
#   output "profile_name" {
   #value = ["${lxd_profile.gui_profile.name}"]
}



resource "lxd_storage_pool" "embed-rust-pool" {
  name = "embed-rust-pool"
  driver = "dir"
  config = {
    source = "/var/snap/projects/terra-lxc/storage_pool/embed-rust-pool"
  }
}


resource "lxd_volume" "embed-rust-volume" {
  name = "${var.volume-name}"
  pool = "${lxd_storage_pool.embed-rust-pool.name}"
}


resource "lxd_network" "embed-rust-net" {
  name = "embed-rust-net"

  config = {
    "ipv4.address" = "${var.ip}" 
    "ipv4.nat"     = "true"
    "ipv6.address" = "fd42:474b:622d:259d::2/64"
    "ipv6.nat"     = "true"
  }
}

resource "lxd_profile" "embed-rust-profile" {

  name = "${var.profile-name}"

  device {
    name = "eth0"
    type = "nic"

    properties = {
      nictype = "bridged"
      parent  = "${lxd_network.embed-rust-net.name}"
    }
  }
  device {
    type = "disk"
    name = "root"

    properties = {
      pool = "${lxd_storage_pool.embed-rust-pool.name}"
      path = "/"
    }
  }

}


resource "lxd_container" "embed-rust" {
  name      = "${var.name}"
  image     = "ubuntu:21.04"
  ephemeral = false
  type      = "container"
  profiles  = ["${var.profile-name}", "${var.gui-profile-name}"] 
  device {
    name = "embed-rust-store"
    type = "disk"
    properties = {
      path = "/opt/"
      source = "${lxd_volume.embed-rust-volume.name}"
      pool = "${lxd_storage_pool.embed-rust-pool.name}"
    }
  }

  device {
    name = "ampserial"
    type = "unix-char"
    properties = {
      path="/dev/ttyUSB0"
/*
      uid = "100000"
      gid = "100020"
      major = "002" 
      minor = "037" 
*/
    }
  }
  device {
    name = "ampsensor"
    type = "usb"
    properties = {
      vendorid = "0403"
      productid = "6001" 
      uid = "100000"
      gid = "100020"
    }
  }
  provisioner "local-exec" {
    command = "echo ${var.name}"
  }

  provisioner "local-exec" {
    command = "lxc file push  ${var.installer} ${var.name}/root/./${var.installer}"
  }

  provisioner "local-exec" {
    command = "lxc exec ${var.name} chmod ugo+x ./${var.installer}" 
  }

  provisioner "local-exec" {
    command = "lxc exec ${var.name} sudo ./${var.installer}"
  }

}
