#! /bin/sh

#
# ./flash.sh target/avr-atmega328p/release/electricity-meter.elf
#

set -e

if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    echo "usage: $0 <path-to-binary.elf>" >&2
    exit 1
fi

if [ "$#" -lt 1 ]; then
    echo "$0: Expecting a .elf file" >&2
    exit 1
fi

cargo build --release
avrdude -v -C/etc/avrdude.conf -patmega328p -carduino -P/dev/ttyUSB0 -b57600 -D  "-Uflash:w:$1:e"
